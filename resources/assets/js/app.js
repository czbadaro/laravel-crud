
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
window.moment = require('moment');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example', require('./components/Example.vue'));

const app = new Vue({
    el: '#app',

    data: {
        customers : [],
        newCustomer : { 'first_name':'', 'last_name':'', 'email':'' , 'gender':'' },
        fillCustomer : { 'first_name':'', 'last_name':'', 'image':'', 'email':'', 'id':'' },
        pagination: {
            total: 0,
            per_page: 2,
            from: 1,
            to: 0,
            current_page: 1,
        },
        offset: 4,
        formErrors: {},
        formErrorsUpdate: {},
    },

    computed: {
        isActived: function() {
            return this.pagination.current_page;
        },

        pagesNumber: function() {
            if (!this.pagination.to) {
                return [];
            }
            var from = this.pagination.current_page - this.offset;
            if (from < 1) {
                from = 1;
            }
            var to = from + (this.offset * 2);
            if (to >= this.pagination.last_page) {
                to = this.pagination.last_page;
            }
            var pagesArray = [];
            while (from <= to) {
                pagesArray.push(from);
                from++;
            }
            return pagesArray;
        }
    },

    mounted() {
        this.getVueCustomers(this.pagination.current_page);
    },

    methods: {
        showAddCustomerModal: function() {
            this.newCustomer.first_name = '';
            this.newCustomer.last_name = '';
            this.newCustomer.email = '';
            this.newCustomer.gender = 'm';
            this.formErrors = '';
        },        

        getVueCustomers: function(page) {
            var that = this;
            axios.get('api/customers?page='+page).then(function (response) {
                that.customers = response.data.data.data;
                that.pagination = response.data.pagination;

                that.$nextTick(function() {
                    $('[data-toggle="popover"]').popover();
                })
            });
        },

        createCustomer: function() {                 
            var input = this.newCustomer;
            var that = this;
            axios.post('api/customers', input).then(function (response) {
                that.getVueCustomers();
                toastr.options = {
                  "timeOut": "2000",
                },
                toastr.success('Customer Added Successfully');
                $(that.$refs.add_customer_modal).on("hidden.bs.modal", that.hideAddCustomerModal());
            })
            .catch(function (error) {
                that.formErrors = error.response.data;
                toastr.options = {
                  "timeOut": "2000",
                },
                toastr.error('Oops! Fill in the required fields!');
            });
        },

        hideAddCustomerModal: function() {
            $(this.$refs.add_customer_modal).modal('hide');
        },

        editCustomer: function(customer) {
            this.fillCustomer = customer;
            this.formErrors = '';
        },

        updateCustomer: function() {
            var input = this.fillCustomer;
            var id = this.fillCustomer.id;
            var that = this;
            axios.patch('api/customer/' + id, input).then(function (response) {
                that.getVueCustomers();
                toastr.options = {
                  "timeOut": "2000",
                },
                toastr.success('Customer Updated Successfully');
                $(that.$refs.add_customer_modal).on("hidden.bs.modal", that.hideEditCustomerModal());
            })
            .catch(function (error) {
                that.formErrors = error.response.data;
                toastr.options = {
                  "timeOut": "2000",
                },
                toastr.error('Oops! Fill in the required fields!');
            });
        },

        hideEditCustomerModal: function() {
            $(this.$refs.edit_customer_modal).modal('hide');
        },

        deleteCustomer: function(id) {
            this.customers = this.customers.filter(function (item) {
                return item.id != id;
            });
            var that = this;
            axios.delete('api/customer/' + id).then(function (response) {
                that.getVueCustomers();
                toastr.options = {
                  "timeOut": "2000",
                },
                toastr.warning('Customer Deleted Successfully');
            });
        },

        changePage: function(page) {
            this.pagination.current_page = page;
            this.getVueCustomers(page);
        },

        moment: function (date) {
            return moment(date);
        },

        date: function (date) {
            return moment(date).format('MMMM Do YYYY, h:mm:ss a');
        },

    },

    filters: {
        moment: function (date) {
            return moment(date).format('L');
        }
    },
    
});


